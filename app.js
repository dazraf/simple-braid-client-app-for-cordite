'use strict';
import { Proxy } from 'braid-client';

let corda;

const url = "https://localhost:9083/api/"

document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    onLoaded()
  }
}

function onLoaded() {
  connect()
}

function connect() {
  corda = new Proxy({ url: url }, onOpen, onClose, onError, { strictSSL: false });
  window.proxy = corda;
}

function onOpen() {
  console.log('opened');
  corda.network.myNodeInfo().then(res => {
    document.getElementById("my-node-info").innerHTML = JSON.stringify(res, null, 2);
  });
}

function onClose() {
  console.log('closed');
}

function onError(e) {
  console.error('error', e);
}
