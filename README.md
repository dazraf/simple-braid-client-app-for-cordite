# Instructions

## Start the cordite test network locally

```bash
cd <cordite-root>/test
./build_env.sh cordite/cordite:latest
```

## Install dependencies

```bash
npm install
```

## Start Server

```bash
npm run watch-server
```

## Browse

[http://localhost:3333](http://localhost:3333)

You should see in the browser a block displayed as such:

```json
{
  "addresses": [
    {
      "host": "amer",
      "port": 10002
    }
  ],
  "legalIdentities": [
    {
      "name": "OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US",
      "owningKey": "GfHq2tTVk9z4eXgyQu8khnRnuYKGrkMzZUscHwg6XNtkgNZQmSiy5f3ZgPg7"
    }
  ]
}
```

## Read my terribly boring code

[app.js](./app.js)

## Note about the version of braid-client

The cordite-ui project is using version `braid-client:^4.1.2-RC13`. This is an old release. I've used it in this project to comply with cordite-ui.

However, we really should be using `@cordite/braid-client`. Current version is `^4.5.3`. I've tested this app with that version, and apart from a small change to the `require` statement, it works. 